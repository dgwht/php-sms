# 短信发送（阿里/腾讯）


> 安装

```bash
composer require dgwht/sms
```



> 短信发送

```php
use dgwht\Sms;

/*
Sms::setConfig('ali',[
    "id" => "LTAI4FdEgtr2kneRWgpSnzQB",
    "key" => "xxxxxxxxx",
    "sign" => "微后台",
    "tpl" => "SMS_138078698",
]);

$ret = Sms::send('18888888888',['data1' => '内容1','data2' => '内容2','data3' => '内容3']);
*/

Sms::setConfig('tx',[
    "id" => "1400158411",
    "key" => "xxxxxxxxx",
    "sign" => "泪心世界",
    "tpl" => "348208",
]);

$ret = Sms::send('18888888888',['内容1','内容2','内容3']);

if($ret === true){
    echo "发送成功";
}else{
    echo "发送失败：[{$ret}]";
}

