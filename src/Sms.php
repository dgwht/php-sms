<?php
namespace dgwht;


class Sms {
	private static $Type;
	private static $Config;
	
	public static function setConfig($type = 'tx', $config=[]) {
	    self::$Type = $type;
	    self::$Config = $config;
	}
	
	public static function send($phone, $data ) {
		$type = strtolower(self::$Type);
		if($type=='tx'){
		    $SMS = new \dgwht\sms\Tx(self::$Config);
		}
		if($type=='ali'){
		    $SMS = new \dgwht\sms\Ali(self::$Config);
		}
		$ret = $SMS->send($phone, $data);
		unset($SMS);
		if($ret !== true){
			return $ret;
		}
		return true;
	}

}